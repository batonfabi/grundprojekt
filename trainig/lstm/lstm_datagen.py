import os
import soundfile
import math
import numpy as np
import logging
import keras
import librosa


class DataGenerator(keras.utils.Sequence):
    'Generates data for Keras'
    __white_list_formats = {'wav'}

    def __init__(self, data_list, classes, batch_size, dim, samplerate, frames, channels, funct, shuffle=True):
        'Initialization'
        self.data_list = data_list
        self.classes = classes
        self.shuffle = shuffle
        self.batch_size = batch_size
        self.frames = frames
        self.samplerate = samplerate
        self.funct = funct
        self.dim = dim
        self.channels = channels
        self.on_epoch_end()

    def get_classes(self):
        return self.classes
    
    #def get_categorical_classes
     #   return self.classes, 

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        if self.shuffle == True:
            np.random.shuffle(self.data_list)

    def __len__(self):
        'Denotes the number of batches per epoch'
        return int(np.floor(len(self.data_list) / self.batch_size))

    def __getitem__(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        batch = self.data_list[index * self.batch_size:(index+1)*self.batch_size]

        # Find list of IDs
        tempy = [e[2] for e in batch]
        tempX = [[e[0], e[1]] for e in batch]
        # Generate data

        x_ret = self.__data_generation(tempX)
        # X = X /self.rescale

        y_ret = keras.utils.to_categorical(tempy, num_classes=len(self.classes))
        return x_ret, y_ret

    def __data_generation(self, x_data):
        'Generates data containing batch_size samples'
        # Initialization
        if self.channels != -1:
            x_ret = np.empty((self.batch_size, *self.dim, self.channels))
        else:
            x_ret = np.empty((self.batch_size, *self.dim))

        # Generate data
        for idx, element in enumerate(x_data):
            # Store sample
            file_path = element[0]
            sample_idx = element[1]

            wav_array, _ = soundfile.read(file_path, int(self.frames*self.samplerate), int(sample_idx), fill_value=0)
            
            x_tmp = self.funct(wav_array)
            if self.channels != -1:
                x_tmp = np.reshape(x_tmp, (*self.dim, self.channels))
            x_ret[idx, ] = x_tmp
        return x_ret


class RawWavDataGenerator():
    'Generates data for Keras'
    __white_list_formats = {'wav'}

    def __init__(self, data_path, slice_length, samplerate, trainingdata_amount, batch_size, channels, shuffle, funct, hop_in_data, debug=False, rewrite_npy=False):
        self.debug = debug
        self.rewrite_npy = rewrite_npy
        self.data_path = data_path
        self.slice_length = slice_length
        self.samplerate = samplerate
        self.trainingdata_amount = trainingdata_amount
        self.batch_size = batch_size
        self.funct = funct
        self.channels = channels
        self.shuffle = shuffle
        self.hop_in_data = hop_in_data
        self.dim = self.get_current_dimension()
        if debug:
            logging.basicConfig(level=logging.DEBUG)
        else:
            logging.basicConfig(level=logging.WARN)

    def get_current_dimension(self):
        y = np.zeros(self.slice_length * self.samplerate)
        ret = self.funct(y)
        return np.shape(ret)

    def get_generators(self):
        data_train, classes = self.get_data_dict()
        data_train, data_test = self.prepare_training_and_test_data(
            data_train, self.trainingdata_amount)

    # def __init__(self, data_list, classes, batch_size, dim, samplerate, frames, channels, funct, shuffle=True):

        train_gen = DataGenerator(data_train, classes, self.batch_size, self.dim,
                                  self.samplerate, self.slice_length, self.channels, self.funct, self.shuffle)
        test_gen = DataGenerator(data_test, classes, self.batch_size, self.dim,
                                 self.samplerate, self.slice_length, self.channels, self.funct, self.shuffle)

        return train_gen, test_gen

    def get_data_dict(self):
        
        data_dict = dict()
        class_list = []
        class_folders = np.sort(os.listdir(self.data_path))
        
        for class_folder in class_folders:
            folder_path = os.path.join(self.data_path, class_folder)
            if os.path.isdir(folder_path):
                meta_folder = os.path.join(folder_path, "meta")
                if not os.path.exists(meta_folder):
                    os.mkdir(meta_folder)

                file_np = os.path.join(meta_folder, "Datalist.npy")
                logging.debug("looking for: " + file_np)
                if not os.path.isfile(file_np) or self.rewrite_npy:
                    if self.rewrite_npy:
                        logging.debug("Refreshing Datalist.npy")
                    else:
                        logging.debug(
                            "Datalist.npy does not exist for " + class_folder)
                    data_dict[class_folder] = []
                    class_list.append(class_folder)
                    class_idx = class_list.index(class_folder)
                    logging.debug("class_idx:" + str(class_idx))
                    cat = keras.utils.to_categorical(class_idx, num_classes=72)
                    logging.debug("class: " + str(class_folder) + "is:" + str(cat))
                    for file_name in os.listdir(folder_path):
                        file_path = os.path.join(folder_path, file_name)

                        if os.path.isfile(file_path) and file_path.endswith("wav"):
                            info = soundfile.info(file_path)

                            if info.samplerate == self.samplerate:
                                x = [[file_path, i] for i in range(0,int(info.duration),self.hop_in_data)]
                                data_dict[class_folder].extend(x)
                            else:
                                raise RuntimeError('samplerate mismatch')
                    logging.debug("creating " + file_np)
                    np.save(file_np, data_dict[class_folder])
                    [ele.append(class_idx) for ele in data_dict[class_folder]]
                else:
                    class_list.append(class_folder)
                    class_idx = class_list.index(class_folder)
                    logging.debug("Datalist.npy exist for " + class_folder)
                    tmp = np.load(file_np)
                    data_dict[class_folder] = tmp.tolist()
                    [ele.append(class_idx) for ele in data_dict[class_folder]]

        return data_dict, class_list

    def prepare_training_and_test_data(self, data_dict, trainingdata_amount):
        """ Prepares the trainings- and test- dataset
            trainingdata_amount: between 0 and 1.0; specifies the amount of trainingsdata in percentage
        """
        train_data = []
        test_data = []
        for class_key in dict(data_dict).keys():
            class_data = data_dict[class_key]
            data_len = len(class_data)
            idx_train_end = int(data_len * trainingdata_amount)
            train_data.extend(class_data[:idx_train_end])
            test_data.extend(class_data[idx_train_end:])
        return train_data, test_data
